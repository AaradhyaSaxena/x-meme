const express = require('express')
const router = express.Router()
const Meme = require('../models/meme')

router.get('/', async (req, res) => {
  let memes
  try {
    memes = await Meme.find().sort({ createdAt: 'desc' }).limit(10).exec()
  } catch {
    memes = []
  }
  res.render('index', { memes: memes })
})

module.exports = router