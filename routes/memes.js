const express = require('express')
const router = express.Router()
const Meme = require('../models/meme')
const User = require('../models/user')
const imageMimeTypes = ['image/jpeg', 'image/png', 'images/gif']

// All Memes Route
router.get('/', async (req, res) => {
  let query = Meme.find()
  try {
    const memes = await query.exec()
    res.render('memes/index', {
      memes: memes,
      searchOptions: req.query
    })
  } catch {
    res.redirect('/')
  }
})

// New Meme Route
router.get('/new', async (req, res) => {
  renderNewPage(res, new Meme())
})

// Create Meme Route
router.post('/', async (req, res) => {
  const meme = new Meme({
    user: req.body.user,
    caption: req.body.caption
  })
  saveCover(meme, req.body.cover)

  try {
    const newMeme = await meme.save()
    res.redirect(`memes/${newMeme.id}`)
  } catch {
    renderNewPage(res, meme, true)
  }
})

// Show Meme Route
router.get('/:id', async (req, res) => {
  try {
    const meme = await Meme.findById(req.params.id)
                           .populate('user')
                           .exec()
    res.render('memes/show', { meme: meme })
  } catch {
    res.redirect('/')
  }
})

// Edit Meme Route
router.get('/:id/edit', async (req, res) => {
  try {
    const meme = await Meme.findById(req.params.id)
    renderEditPage(res, meme)
  } catch {
    res.redirect('/')
  }
})

// Update Meme Route
router.put('/:id', async (req, res) => {
  let meme

  try {
    meme = await Meme.findById(req.params.id)
    meme.user = req.body.user
    meme.caption = req.body.caption
    if (req.body.cover != null && req.body.cover !== '') {
      saveCover(meme, req.body.cover)
    }
    await meme.save()
    res.redirect(`/memes/${meme.id}`)
  } catch {
    if (meme != null) {
      renderEditPage(res, meme, true)
    } else {
      redirect('/')
    }
  }
})

// Delete Meme Page
router.delete('/:id', async (req, res) => {
  let meme
  try {
    meme = await Meme.findById(req.params.id)
    await meme.remove()
    res.redirect('/memes')
  } catch {
    if (meme != null) {
      res.render('memes/show', {
        meme: meme,
        errorMessage: 'Could not remove meme'
      })
    } else {
      res.redirect('/')
    }
  }
})

async function renderNewPage(res, meme, hasError = false) {
  renderFormPage(res, meme, 'new', hasError)
}

async function renderEditPage(res, meme, hasError = false) {
  renderFormPage(res, meme, 'edit', hasError)
}

async function renderFormPage(res, meme, form, hasError = false) {
  try {
    const users = await User.find({})
    const params = {
      users: users,
      meme: meme
    }
    if (hasError) {
      if (form === 'edit') {
        params.errorMessage = 'Error Updating Meme'
      } else {
        params.errorMessage = 'Error Creating Meme'
      }
    }
    res.render(`memes/${form}`, params)
  } catch {
    res.redirect('/memes')
  }
}

function saveCover(meme, coverEncoded) {
  if (coverEncoded == null) return
  const cover = JSON.parse(coverEncoded)
  if (cover != null && imageMimeTypes.includes(cover.type)) {
    meme.coverImage = new Buffer.from(cover.data, 'base64')
    meme.coverImageType = cover.type
  }
}

module.exports = router