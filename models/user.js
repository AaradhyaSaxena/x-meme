const mongoose = require('mongoose')
const Meme = require('./meme')

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  }
})

userSchema.pre('remove', function(next) {
  Meme.find({ user: this.id }, (err, memes) => {
    if (err) {
      next(err)
    } else if (memes.length > 0) {
      next(new Error('This user has memes still'))
    } else {
      next()
    }
  })
})

module.exports = mongoose.model('User', userSchema)